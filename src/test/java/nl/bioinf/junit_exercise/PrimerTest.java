package nl.bioinf.junit_exercise;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrimerTest {
    @Test
    public void getGcPercentageSunny() throws Exception {
        final String inputSequence = "actg";
        final Double expectedGCPercentage = 0.50;
        Primer test = new Primer("actg");
        final Double givenGCPercentage = test.getGcPercentage();
        assertEquals(expectedGCPercentage, givenGCPercentage);
    }

    @Test (expected = NullPointerException.class)
    public void getGcPercentageNull() throws Exception {
        final String inputSequence = null;
        Primer test = new Primer(inputSequence);
        final Double givenGCPercentage = test.getGcPercentage();
//        assertEquals(expectedGCPercentage, givenGCPercentage);
    }

    @Test (expected = IllegalArgumentException.class)
    public void getGcPercentageEmpty() throws Exception {
        final String inputSequence = "";
        Primer test = new Primer(inputSequence);
        final Double givenGCPercentage = test.getGcPercentage();
    }


    @Test
    public void getMeltingTemperatureSunny() throws Exception {
        final String inputSequence = "actg";
        final Double expectedMeltingTemperature = 12.0;
        final Double givenMeltingTemperature = new Primer(inputSequence).getMeltingTemperature();
        assertEquals(expectedMeltingTemperature, givenMeltingTemperature);
    }

    @Test (expected = NullPointerException.class)
    public void getMeltingTemperatureNull() throws Exception {
        final String inputSequence = null;
        final Double expectedGCPercentage = 0.50;
        final Double givenMeltingTemperature = new Primer(inputSequence).getMeltingTemperature();
    }

    @Test
    public void getMeltingTemperatureEmpty() throws Exception {
        final String inputSequence = "";
        final Double expectedGCPercentage = 0.0;
        final Double givenMeltingTemperature =  new Primer(inputSequence).getMeltingTemperature();

    }

}