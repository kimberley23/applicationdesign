package nl.bioinf.junit_exercise;

import org.junit.Test;

import static org.junit.Assert.*;

public class SphereTest {
    @Test
    public void getRadiusSunny() throws Exception {
        Double expectedRadius = 15.25;
        Double givenRadius = new Sphere(30.5).getRadius();
        assertEquals(expectedRadius, givenRadius);
    }

    @Test (expected = NullPointerException.class)
    public void getRadiusNull() throws Exception {
        Double givenRadius = new Sphere(null).getRadius();
    }

    @Test
    public void getRadiusEmpty() throws Exception {
        Double expectedRadius = 0.0;
        Double givenRadius = new Sphere(0.0).getRadius();
        assertEquals(expectedRadius, givenRadius);
    }

    @Test
    public void getSurfaceAreaSunny() throws Exception {
        Double expectedSurface = 730.62;
        Double givenSurface = new Sphere(30.5).getSurfaceArea();
        assertEquals(expectedSurface, givenSurface, .01);
    }

    @Test (expected = NullPointerException.class)
    public void getSurfaceAreaNull() throws Exception {
        Double givenSurface = new Sphere(null).getSurfaceArea();
    }

    @Test
    public void getSurfaceAreaEmpty() throws Exception {
        Double expectedRadius = 0.0;
        Double givenRadius = new Sphere(0.0).getSurfaceArea();
        assertEquals(expectedRadius, givenRadius);
    }

    @Test
    public void getVolumeSunny() throws Exception {
        Double expectedVolume = 14855.87;
        Double givenVolume = new Sphere(30.5).getVolume();
        assertEquals(expectedVolume, givenVolume, .01);
    }

    @Test (expected = NullPointerException.class)
    public void getVolumeNull() throws Exception {
        Double givenVolume = new Sphere(null).getVolume();
    }

    @Test
    public void getVolumeEmpty() throws Exception {
        Double expectedVolume = 0.0;
        Double givenVolume = new Sphere(0.0).getVolume();
        assertEquals(expectedVolume, givenVolume);
    }

}