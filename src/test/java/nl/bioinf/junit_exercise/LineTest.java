package nl.bioinf.junit_exercise;

import org.junit.Test;

import static org.junit.Assert.*;

public class LineTest {
    @Test
    public void getSlopeSunny() throws Exception {
        Double expectedSlope = -2.0;
        Double givenSlope = new Line(3,8,6,2).getSlope();
        assertEquals(expectedSlope, givenSlope, .01);
    }

    @Test (expected = ArithmeticException.class)
    public void getSlopeRainy() throws Exception {
        Double givenSlope = new Line(3,8,3,2).getSlope();
    }

    @Test (expected = ArithmeticException.class)
    public void getSlopeEmpty() throws Exception {
        Double givenSlope = new Line(0,0,0,0).getSlope();
    }

    @Test
    public void getDistanceSunny() throws Exception {
        Double expectedDistance = 6.71;
        Double givenDistance = new Line(3,8,6,2).getDistance();
        assertEquals(expectedDistance, givenDistance, .01);

    }

    @Test
    public void getDistanceZero() throws Exception {
        Double expectedDistance = 0.00;
        Double givenDistance = new Line(0,0,0,0).getDistance();
        assertEquals(expectedDistance, givenDistance, .01);

    }

    @Test
    public void parallelToSunny() throws Exception {
        Boolean expectedParallel = true;
        Line line = new Line(3,10,6,4);
        Boolean givenParallel = new Line(3,8,6,2).parallelTo(line);
        assertEquals(expectedParallel, givenParallel);
    }

    @Test
    public void parallelToRainy() throws Exception {
        Boolean expectedParallel = false;
        Line line = new Line(3,10,6,4);
        Boolean givenParallel = new Line(8,8,6,2).parallelTo(line);
        assertEquals(expectedParallel, givenParallel);
    }

    @Test (expected = ArithmeticException.class)
    public void parallelToZero() throws Exception {
        Boolean expectedParallel = true;
        Line line = new Line(0,0,0,0);
        Boolean givenParallel = new Line(0,0,0,0).parallelTo(line);
    }
}