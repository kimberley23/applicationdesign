package nl.bioinf.junit_exercise;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrimerFilterTest {
    @Test
    public void isOKSunny() throws Exception {
        Primer primer = new Primer("actg");
        final Boolean expectedValue = true;
        final Boolean givenValue = new HomopolymerFilter().isOK(primer);
        assertEquals(givenValue, expectedValue);
    }

    @Test
    public void isOKRainy() throws Exception {
        Primer primer = new Primer("actgkim");
        final Boolean expectedValue = false;
        final Boolean givenValue = new HomopolymerFilter().isOK(primer);
        assertEquals(givenValue, expectedValue);
    }

    @Test
    public void isOKNull() throws Exception {
        Primer primer = new Primer("");
        final Boolean expectedValue = false;
        final Boolean givenValue = new HomopolymerFilter().isOK(primer);
        assertEquals(givenValue, expectedValue);
    }

    @Test (expected = NullPointerException.class)
    public void isOKEmpty() throws Exception {
        Primer primer = new Primer(null);
        final Boolean givenValue = new HomopolymerFilter().isOK(primer);
    }
}