package nl.bioinf.junit_exercise;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.Assert.*;


public class PrimerTest2 {
    @Test
    public void getGcPercentage() throws Exception {
    }

    @ParameterizedTest(name="{index} => seq={0}, expected={1}")
    @CsvSource( {"actg, 0.50", "a, IllegalArgumentException.class", "abcdegggggt, 0.55", "gggccc, 1.00", "acc, 0.67"})
    void getGcPercentage(String sequence, Double expectedGcPercentage) throws Exception {
        Double gcPercentage = new Primer(sequence).getGcPercentage();
        assertEquals(gcPercentage, expectedGcPercentage, .01);
    }

}