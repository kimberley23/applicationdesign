package nl.bioinf.junit_exercise;

public class Sphere {
    final private double diameter;

    public Sphere(Double diameter){
        this.diameter = diameter;
    }

    public Double getRadius() {
        Double radius = this.diameter/2;
        return radius;
    }

    public Double getSurfaceArea() {
        Double surface = Math.PI * Math.pow(getRadius(), 2);
        return surface;
    }

    public Double getVolume() {
        Double volume = (4.0/3.0) * Math.PI * Math.pow(getRadius(), 3);
        return volume;
    }

    public double getDiameter() {
        return diameter;
    }
}
