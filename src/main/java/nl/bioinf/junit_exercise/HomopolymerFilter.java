package nl.bioinf.junit_exercise;

import java.util.Arrays;
import java.util.HashSet;

public class HomopolymerFilter implements PrimerFilter {
    @Override
    public boolean isOK(Primer primer) {
        HashSet<Character> nucleotides = new HashSet<Character>(Arrays.asList('A', 'C', 'G', 'T'));
        HashSet<Character> set = new HashSet<Character>();
        for(char c : primer.getSequence().toCharArray()) {
            set.add(c);
        }

        if (set.equals(nucleotides)) {
            return true;
        }
        return false;
    }

    @Override
    public String getName() {
        return null;
    }
}
