package nl.bioinf.designpatterns.singleton;

/**
 * Creation date: Jul 08, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class ClassicSingleton {

    private static ClassicSingleton instance;

    /**
     * private constructor ensures no one can
     * instantiate it beside its own class!
     */
    private ClassicSingleton() {
    }

    /**
     * the only means to get hold of the instance.
     *
     * @return
     */
    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }

}
