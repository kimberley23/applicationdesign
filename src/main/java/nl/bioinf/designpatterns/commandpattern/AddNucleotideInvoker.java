package nl.bioinf.designpatterns.commandpattern;

import java.util.Stack;

/**
 * Creation date: Jul 07, 2017
 *
 * @author Michiel Noback (&copy; 2017)
 * @version 0.01
 */
public class AddNucleotideInvoker {
    private Stack<Command> commands = new Stack<>();

    public void addCommand(Command command) {
        command.execute();
        this.commands.push(command);
    }

    public void undoCommand() {
        Command undo = this.commands.pop();
        undo.undo();
    }
}
